# dmenu-hentaigana

A simple hiragana and hentaigana input based on dmenu.

It requires [dmenu](https://tools.suckless.org/dmenu/) (or [xmenu](https://github.com/uluyol/xmenu) on macOS).

File `hentaiganalist.txt` contains a list of hentaigana (and hiragana) Unicode symbols (in utf-8 encoding) with corresponding keys used to search and input each symbol. Put it somewhere on your system.

Keys’ format is Nippon-shiki Rōmaji (e.g. `si` for し, `tu` for つ, ) followed by a hexadecimal number. Number 0 corresponds to hiragana, and sequential numbers correspond to hentaigana variants. For example, haB stands for 𛂨 which is a variant of は that originates from a handwritten form of character 頗.

Note that _dakuon_ and _handakuon_ are currently absent from the list, therefore, keys `b0` and `p0` are reserved for _dakuten_ and _handakuten_ symbols, so in rare case you need to add them, you may input them after you input a regular symbol.

Notice that in order to see the hentaigana, you should install a font that contains the corresponding symbols, for example, [UniHentaiKana](https://gigazine.net/news/20170627-unicode-hentaikana-font/).

In order to input a character, run the following command in the terminal:

`/path/to/dmenu < /path/to/hentaiganalist.txt | awk '{printf "%s", $2}'`

This will show all characters using dmenu. Start typing the romaji and the variants will be filtered to include only those corresponding to the given character. Finally, input a number or use arrows to choose the desired character and press Enter (Return) to input a character, or Escape to quit dmenu.

You may also use this command with any text editor that can insert a result of a shell command into the file.

For example, if you use vim, you might put the following line in your vimrc file:

`inoremap ,j <C-r>=system("dmenu < /path/to/hentaiganalist.txt \| awk '{printf \"\%s\", $2}'")<CR>`
  
Then, whenever you are in insert mode, you can invoke dmenu by pressing `,j`, and after you choose the character, it will be inserted at cursor.